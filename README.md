# Messenger App

My first dynamic Rails app based on the course Learn Ruby on Rails from Codecademy.

## Installation

Run `bundle install` to install the project dependencies.

## Starting the app

Run `rails server` to start the app locally.

## Configuring the database

Run `rake db:migrate && rake db:seed` to migrate and seed the database.

### Accessing the app locally

After the rails server is started and the database is configured, access the following URL in your preferable web browser: http://localhost:3000/messages

While navigating through the app you should see something like the below samples.

![Messenger App Home 1](./messenger-app-home-1.png)
![Messenger App New 1](./messenger-app-new-1.png)
![Messenger App New 2](./messenger-app-new-2.png)
![Messenger App Home 2](./messenger-app-home-2.png)